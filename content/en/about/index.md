---
title: "About"
description: "Creating individual software solutions exactly matching your needs"
featured_image: 'business-5475661_1280.jpg'
menu:
  main:
    weight: 1
---

Not all requirements can be met by software from the shelf. We're creating individual software solutions to match exactly your needs.

This is just a fictional brand for private purposes. We don't really do any business. 
We don't collect or process your private data on this page.

Michael Voggenreiter
Lugauer Weg 7
90522 Oberasbach
[michael.voggenreiter@byte-blends.de](mailto:michael.voggenreiter@byte-blends.de)